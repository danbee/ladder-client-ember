import Ember from 'ember';
let $ = Ember.$;

export default function(el, opts) {
  opts = opts || {};
  const component = $(el);
  const flexiblesSelector = opts.flexibles;
  const base = opts.base ? component.find(opts.base) : component;
  const container = opts.container || window;

  return function() {
    const availableH = $(container).height();
    const parts = base.children();
    const flexibles = parts.filter(flexiblesSelector);
    const fixies = parts.not(flexibles);
    base.height(availableH);
    const fixedH = fixies.toArray()
      .map(f => $(f).outerHeight())
      .reduce((sum,h) => sum + h, 0);
    const roomForFits = availableH - fixedH;
    flexibles.height(roomForFits/flexibles.length);
  };
}

