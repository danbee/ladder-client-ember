import Ember from 'ember';

export default Ember.Component.extend({
  announcer: Ember.inject.service('result-announcer'),

  isPresent: Ember.computed.bool('announcer.latestResult'),

  result: Ember.computed.alias('announcer.latestResult'),
  winnerName: Ember.computed.alias('result.winner.name'),
  loserName: Ember.computed.alias('result.loser.name'),
  transfer: Ember.computed.alias('result.transfer'),
});
