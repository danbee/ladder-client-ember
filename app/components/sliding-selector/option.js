import Ember from 'ember';

export default Ember.Component.extend({
  code: null,
  selected: false,

  classNameBindings: ['selected:sliding-selector-selected'],

  click() {
    this.sendAction('onSelected', this.get('code'));
  },
});
