import Ember from 'ember';

export default Ember.Component.extend({
  results: [],

  resultsSorting: ['createdAt:desc'],
  orderedResults: Ember.computed.sort('results', 'resultsSorting'),
});
