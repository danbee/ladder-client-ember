import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'li',
  classNames: 'nav-panel-action',

  route: null,
  title: null,
  icon: null,

  linkClassNames: Ember.computed('icon', function(){
    let str = 'btn';
    const icon = this.get('icon');
    if (icon) {
      str += ' app-icon app-icon-' + icon;
    }
    return str;
  }),
});
