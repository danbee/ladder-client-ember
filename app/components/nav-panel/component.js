import Ember from 'ember';
import buildResizeHandler from 'ladder-client/lib/build-resize-handler';
const $ = Ember.$;

export default Ember.Component.extend({
  classNames: 'nav-panel',

  didInsertElement() {
    this.adjustVBox = buildResizeHandler(this.element, {
      flexibles: '.nav-panel-content',
    });
    $(window).on('resize', this.adjustVBox);
  },

  didRender() {
    this.adjustVBox();
  },

  willDestroyElement() {
    $(window).off('resize', this.adjustVBox);
  },
});
