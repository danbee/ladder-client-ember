import Ember from 'ember';

export default Ember.Component.extend({
  player: null,

  isValid: Ember.computed.not('isInvalid'),
  isInvalid: Ember.computed.empty('player.name'),

  actions: {
    save() {
      this.sendAction('save');
    }
  },
});
