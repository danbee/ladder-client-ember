import Ember from 'ember';

function playersWithRank(players) {
  let lastScore = null;
  let lastRank = 0;
  return players.sortBy('score').reverse().map((player, i) => {
    const curScore = player.get('score');
    lastRank = curScore === lastScore ? lastRank : i+1;
    player.set('rank', lastRank);
    lastScore = curScore;
    return player;
  });
}

export default Ember.Route.extend({
  model() {
    return this.store.findAll('player')
      .then(players => playersWithRank(players));
  },
});
