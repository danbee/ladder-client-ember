import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.createRecord('player');
  },

  actions: {
    save() {
      const record = this.modelFor('players.new');
      record.save()
        .then(() => {
          this.transitionTo('root.show');
        })
        .catch(error => {
          console.error("Error saving player", error);
        });
    },

    willTransition() {
      const record = this.controllerFor('players.new').get('model');
      if (record.get('isNew')) {
        return record.destroyRecord();
      }
    },
  },
});
