import DS from 'ember-data';

export default DS.Model.extend({
  transfer: DS.attr('number'),
  createdAt: DS.attr('date'),

  winner: DS.belongsTo('player', {inverse: 'wins'}),
  loser: DS.belongsTo('player', {inverse: 'loses'}),
});
