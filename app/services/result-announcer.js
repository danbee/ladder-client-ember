import Ember from 'ember';

export default Ember.Service.extend({
  latestResult: null,

  announce: function(result) {
    this.set('latestResult', result);
  },

  isSet: Ember.computed.bool('latestResult'),
});
