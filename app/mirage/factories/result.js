import Mirage, { faker } from 'ember-cli-mirage';

export default Mirage.Factory.extend({
  transfer: 10,
  createdAt: faker.date.recent,
});
