export default {
  open() {
    click('.bar-nav .modal-opener');
  },

  visit(routeName) {
    const selector = `[data-dst="${routeName}"]`;
    click(selector);
    andThen(function() {
    });
  },
};
