import { test } from 'qunit';
import moduleForAcceptance from 'ladder-client/tests/helpers/module-for-acceptance';

function map(array, callback) {
  return Array.prototype.map.call(array, callback);
}

function listedNames() {
  return map(find('.ladder-rankings-name'), rn => $(rn).text().trim());
}

moduleForAcceptance('Acceptance | create player');

test('Create a player', function(assert) {
  visit('/');

  click('[data-dst="players.new"]');
  andThen(function() {
    fillIn('input[name="name"]', "Alice");
    click('button');
  });
  andThen(function() {
    assert.deepEqual(listedNames(), ['Alice']);
    assert.equal(server.db.players.length, 1);
  });
});

test("Cancel creating a player", function(assert) {
  visit('/');

  click('[data-dst="players.new"]');
  andThen(function() {
    click('[data-dst="root.show"]');
  });
  andThen(function() {
    assert.deepEqual(listedNames(), []);
    assert.equal(server.db.players.length, 0);
  });
});
