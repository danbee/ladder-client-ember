import { moduleFor, test } from 'ember-qunit';
import Result from 'ladder-client/models/result';

moduleFor('serializer:application', 'Unit | Serializer | application', {
  // Specify the other units that are required for this test.
  needs: ['serializer:application']
});

test("reads underscores as camel case", function(assert) {
  const s = this.subject();
  const modelClass = Result;
  const resourceHash = {
    type: 'results',
    id: '1',
    attributes: {
      created_at: '2015-11-22T11:05:58.999Z',
    },
  };

  const result = s.normalize(modelClass, resourceHash);
  assert.deepEqual(Object.keys(result.data.attributes), ['createdAt']);
});
