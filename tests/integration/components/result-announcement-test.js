import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('result-announcement', 'Integration | Component | result announcement', {
  integration: true,

  beforeEach: function() {
  },
});

test("by default, it doesn't render", function(assert) {
  const Announcer = Ember.Object.extend({
    latestResult: null,
  });
  this.register('service:result-announcer', Announcer);

  this.render(hbs`{{result-announcement}}`);
  assert.equal(this.$().text().trim(), '');
});

test("when there's a result, it renders", function(assert) {
  const result = Ember.Object.create({
    winner: { name: 'foo' },
    loser: { name: 'bar' },
    transfer: 10,
  });
  const Announcer = Ember.Object.extend({
    latestResult: result,
  });
  this.register('service:result-announcer', Announcer);

  this.render(hbs`{{result-announcement}}`);
  const actual = this.$().text().replace(/\s+/g, " ").trim();
  const expected = "foo won 10 points from bar";
  assert.equal(actual, expected);
});
